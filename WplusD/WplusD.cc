// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/DirectFinalState.hh"

#include <unordered_map>
#include <iostream>
namespace Rivet {

  /// @brief W+D 
  class WplusD : public Analysis {
  public:

    RIVET_DEFAULT_ANALYSIS_CTOR(WplusD);

    // Book histograms and initialise before the run
    void init() {
      const FinalState fs;
      DirectFinalState photons(Cuts::abspid == PID::PHOTON);

      DirectFinalState bare_leps(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON);

      // Dress the bare direct leptons with direct photons within dR < 0.1,
      // and apply some fiducial cuts on the dressed leptons
      Cut lepton_cuts = Cuts::abseta < 2.5 && Cuts::pT > 20*GeV;
      DressedLeptons dressed_leps(photons, bare_leps, 0.1, lepton_cuts);
      declare(dressed_leps, "leptons");
      // Missing momentum
      declare(MissingMomentum(fs), "MET");

      // unstable final-state for Ds
      declare(UnstableParticles(), "UFS");

      std::vector<std::string> chargeChannels = {"OS", "SS", "OS-SS"};

      for(const auto &ch : chargeChannels){
	book(_h[ch + "_D_pt"], ch + "_D_pt",20, 0.0, 100.0);
	book(_h[ch + "_D_abs_eta"],ch + "_D_abs_eta",25,0,2.5);
	book(_h[ch + "_lep_pt"], ch + "_lep_pt",20, 0.0, 100.0);
	book(_h[ch + "_lep_abs_eta"],ch + "_lep_abs_eta",25,0,2.5);
	book(_h[ch + "_W_pt"], ch + "_W_pt",20, 0.0, 100.0);
	book(_h[ch + "_met"],ch + "_met",20,0,100);
	book(_h[ch + "_dR_D_lep"],ch + "_dR_D_lep",20,0,4);
	book(_h[ch + "_dPhi_D_met"],ch + "_dPhi_D_met",20,0,4);
	book(_h[ch + "_dR_D_W"],ch + "_dR_D_W",20,0,4);
	book(_h[ch + "_D_pt_over_W_pt"],ch + "_D_pt_over_W_pt",30,0,3.);
	book(_h[ch + "_D_pt_over_lep_pt"],ch + "_D_pt_over_lep_pt",30,0,3.);
	book(_h[ch + "_D_pt_over_met"],ch + "_D_pt_over_met",30,0,3.);
      }
    }

    /// Perform the per-event analysis
    void analyze(const Event &event) {
      // Retrieve the dressed electrons
      const auto leptons = apply<FinalState>(event, "leptons").particles();
      if (leptons.size() !=1 ) vetoEvent;
      const Particle &lepton = leptons[0];
      const auto met = apply<MissingMomentum>(event, "MET").missingPt();
      const auto met_p = apply<MissingMomentum>(event, "MET");
      auto recoW = lepton.momentum().vector3() + met_p.vectorEtMiss();

      // Get the charm hadrons
      const UnstableParticles &ufs = apply<UnstableFinalState>(event, "UFS");
      std::unordered_map<unsigned int, Particles> chadrons;

      // Loop over particles
      for (const Particle &p : ufs.particles()) {

        const int id = p.abspid();
        const double pt = p.pT() / GeV;
        const double eta = p.abseta();
        if (_charm_hadron_names.count(id) && pt > 8.0 && eta < 2.2) {
          chadrons[id].push_back(p);
        }
      }

      // Fill histograms
      for (auto &kv : chadrons) {
        const unsigned int absPdgId = kv.first;

        const std::string hadron_name = _charm_hadron_names.at(absPdgId);
        for (auto &p : kv.second) {
          // Weight: +1 for OS and -1 for SS
          float charm_charge = (absPdgId == 421) ? p.pid() : p.charge();
	  bool isOS = charm_charge*lepton.charge() < 0;	  
          double weight = isOS ? +1.0 : -1.0;

          // Fill only D+
          if (absPdgId != PID::DPLUS )  continue;	  

	  std::string chargeChannel = isOS ? "OS" : "SS";

	  _h[chargeChannel + "_D_pt"]->fill(p.pT() / GeV);
	  _h[chargeChannel + "_D_abs_eta"]->fill(p.abseta());
	  _h[chargeChannel + "_lep_pt"]->fill(lepton.pT()/GeV);
	  _h[chargeChannel + "_lep_abs_eta"]->fill(lepton.eta());

	  _h[chargeChannel + "_W_pt"]->fill( recoW.perp() / GeV);
	  _h[chargeChannel + "_met"]->fill( met / GeV);
	  _h[chargeChannel + "_dR_D_lep"]->fill( deltaR(p, lepton) );
	  _h[chargeChannel + "_dPhi_D_met"]->fill( deltaPhi(p, met_p.vectorEtMiss()));
	  _h[chargeChannel + "_dR_D_W"]->fill( deltaR( p, recoW ));
	  _h[chargeChannel + "_D_pt_over_W_pt"]->fill( (p.pT()/GeV) / (recoW.perp()/GeV ));
	  _h[chargeChannel + "_D_pt_over_lep_pt"]->fill( (p.pT()/GeV) / (lepton.pT()/GeV) );
	  _h[chargeChannel + "_D_pt_over_met"]->fill( (p.pT()/GeV) / (met/GeV) );

	  _h["OS-SS_D_pt"]->fill(p.pT()/GeV, weight);
	  _h["OS-SS_D_abs_eta"]->fill(p.abseta());
	  _h["OS-SS_lep_pt"]->fill(lepton.pT()/GeV,weight);
	  _h["OS-SS_lep_abs_eta"]->fill(lepton.eta(),weight);

	  _h["OS-SS_W_pt"]->fill( recoW.perp() / GeV);
	  _h["OS-SS_met"]->fill( met / GeV);
	  _h["OS-SS_dR_D_lep"]->fill( deltaR(p, lepton) );
	  _h["OS-SS_dPhi_D_met"]->fill( deltaPhi(p, met_p.vectorEtMiss()));
	  _h["OS-SS_dR_D_W"]->fill( deltaR( p, recoW ));
	  _h["OS-SS_D_pt_over_W_pt"]->fill( (p.pT()/GeV) / (recoW.perp()/GeV ));
	  _h["OS-SS_D_pt_over_lep_pt"]->fill( (p.pT()/GeV) / (lepton.pT()/GeV) );
	  _h["OS-SS_D_pt_over_met"]->fill( (p.pT()/GeV) / (met/GeV) );

	}
      } // chadrons
    } // analyze

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Finalize
    void finalize() {

      scale(_h, crossSectionPerEvent());

    }// finalize
 
  private:

    // Mapping between pdg id an charm hadron names
    const std::unordered_map<unsigned int, std::string> _charm_hadron_names = {
      {PID::DPLUS, "Dplus"},
      {PID::DSTARPLUS, "Dstar"},
      {PID::D0, "Dzero"},
      {PID::DSPLUS, "Ds"},
      {PID::LAMBDACPLUS, "LambdaC"},
      {4132 /*PID::XI0C*/, "XiCzero"},
      {4232 /*PID::XICPLUS*/, "XiCplus"},
      {4332 /*PID::OMEGA0C*/, "OmegaC"},
    };

    // Histogram map
    map<string, Histo1DPtr> _h;

  };

  RIVET_DECLARE_PLUGIN(WplusD);
}
